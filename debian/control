Source: pulseaudio-module-xrdp
Section: sound
Priority: optional
Maintainer: Kali Developers <devel@kali.org>
Uploaders: Arnaud Rebillout <arnaudr@kali.org>
Build-Depends:
 debhelper-compat (= 13),
 libpulse-dev,
# Needed to configure pulseaudio
 meson,
 libasound2-dev [linux-any],
 libasyncns-dev,
 libavahi-client-dev,
 libbluetooth-dev [linux-any] <!stage1>,
 libsbc-dev [linux-any],
 libcap-dev [linux-any],
 libfftw3-dev,
 libglib2.0-dev,
 libgstreamer1.0-dev (>= 1.14),
 libgstreamer-plugins-base1.0-dev,
 libgtk-3-dev,
 libice-dev,
 libjack-dev,
 liblirc-dev,
 libltdl-dev,
 liborc-0.4-dev,
 libsndfile1-dev,
 libsoxr-dev,
 libspeexdsp-dev,
 libssl-dev,
 libsystemd-dev [linux-any],
 libtdb-dev,
 libudev-dev [linux-any],
 libwebrtc-audio-processing-dev [linux-any],
 libwrap0-dev,
 libx11-xcb-dev,
 libxcb1-dev,
 libxtst-dev,
Standards-Version: 4.6.2
Homepage: https://github.com/neutrinolabs/pulseaudio-module-xrdp
Vcs-Browser: https://gitlab.com/kalilinux/packages/pulseaudio-module-xrdp
Vcs-Git: https://gitlab.com/kalilinux/packages/pulseaudio-module-xrdp.git
Rules-Requires-Root: no

Package: pulseaudio-module-xrdp
Architecture: any
Depends:
 ${misc:Depends},
 ${pulseaudio:Depends},
 ${shlibs:Depends},
Description: xrdp module for PulseAudio sound server
 PulseAudio, previously known as Polypaudio, is a sound server for POSIX and
 WIN32 systems. It is a drop in replacement for the ESD sound server with
 much better latency, mixing/re-sampling quality and overall architecture.
 .
 This modules provides xrdp sink / source for PulseAudio.
 .
 The server to client audio redirection is implemented as per Remote Desktop
 Protocol: Audio Output Virtual Channel Extension [MS-RDPEA] specs, which
 means it is interoperable with any RDP client which implements it (most of
 them including: MS RDP clients, FreeRDP).
 .
 The client to server audio redirection is implemented as per Remote Desktop
 Protocol: Audio Input Redirection Virtual Channel Extension [MS-RDPEAI] which
 means it is interoperable with any RDP client which implements it (most of
 them including: MS RDP clients, FreeRDP).
 .
 The module is called module-xrdp.
